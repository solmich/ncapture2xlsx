import os
import random
import time
import uuid
import pandas as pd
from bs4 import BeautifulSoup
from zipfile import ZipFile
from flask import Flask, request, render_template, session, flash, redirect, \
    url_for, jsonify, Markup


app = Flask(__name__)
app.config['SECRET_KEY'] = 'top-secret!'

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'GET':
        return render_template('index.html')

    upload = request.files['input_file']
    if upload.filename != '':
        ext = upload.filename.split('.')[-1]
        fname = str(uuid.uuid4())+'.'+ext
        fname2 = fname.replace('.'+ext,'.xlsx')
        upload.save('static/'+fname)
        if ext == 'zip':
            with ZipFile('static/'+fname) as fzip:
                f = fzip.open(fzip.namelist()[0]).read().decode('utf-16')
        else:
            f = open('static/'+fname, encoding='latin-1').read()
        
        f = f.replace('\x00','')
        tmp = BeautifulSoup(f, 'lxml')
        cols = []
        for i in tmp.find_all('heading'):
            cols.append(i.text)
        all_rows = []
        for rows in tmp.find_all('row'):
            row = []
            for col in rows.find_all('column'):
                row.append(col.text)
            all_rows.append(row)
        df = pd.DataFrame(all_rows, columns=cols)
        df.to_excel('static/'+fname2, index=False)
        flash(Markup(f'Your excel file is ready. >>>> <a href="static/{fname2}">Download</a> <<<<'))
        
    return redirect(url_for('index'))


if __name__ == '__main__':
    app.run(debug=True)
