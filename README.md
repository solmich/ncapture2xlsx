# NCapture by NVIVO to Excel
NCapture extension for Chrome based web browser is very useful to collect data from social media, alas, the captured file only can be processed using NVIVO software, yet it can't nor don't provide any options to export the dataset into other general format such as CSV.

This is a very simple tool to hack the ncvx file format and decode it by it's fixed heading columns, thus enable us to save into XLSX file for further processing.

Why XLSX you asked.... Well, captured format usually is very large (tens to hundreds megabytes of size), so CSV is not a good format filesize-wise. XLSX itself actually is a ZIP file, so let's be it....

